
let {HttpError} = require('../error');

exports.auth = function(req, res, next) {
    let secret = 'key';
    if (!req.headers['header'] || req.headers['header'] !== secret) {
      next(new HttpError(401, 'Authorization failed'));
    }
    next();
};
