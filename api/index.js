const express = require('express');
let app = module.exports = express();

let apiV1 = require('./v1');

app.use('/', apiV1);
