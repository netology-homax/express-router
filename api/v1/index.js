const express = require('express');
let app = module.exports = express();

let helloApi = require('./hello');
let subApi = require('./sub');
let postApi = require('./post');

app.get('/', function(req, res) {
    res.send('Hello Express.js');
});

app.use('/hello', helloApi);
app.use('/sub', subApi);
app.use('/post', postApi);
