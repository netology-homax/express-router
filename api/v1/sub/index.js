const express = require('express');
let app = module.exports = express();

app.get('/*', function(req, res) {
  res.send(`You requested URI: ${req.originalUrl}`);
});
