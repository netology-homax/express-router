const express = require('express');
let app = module.exports = express();

app.get('/', function(req, res) {
  res.send('Hello stranger !');
});

app.get('/:name', function(req, res) {
  res.send(`Hello ${req.params.name} !`);
});
