const express = require('express');
let app = module.exports = express();

let {auth} = require('../../../middlewares');

app.use(auth);

app.post('/', function(req, res, next) {
  if (Object.keys(req.body).length > 0) {
    res.json(req.body);
    return;
  }
  next();
});
