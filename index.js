const express = require('express');
const bodyParser = require('body-parser');
let app = express();

let {HttpError} = require('./error');
const api = require('./api');

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use('/', api);

app.use(function(req, res) {
  res.type('text/plain');
  res.status(404);
  res.send('404 Page not found');
});

app.use(function(err, req, res, next) {
  if (err instanceof HttpError) {
    res.status(err.status);
    res.end(err.message);
  } else {
    res.type('text/plain');
    res.status(500);
    res.end('500 - Internal server error');
  }
});

app.listen(app.get('port'), function() {
  console.log(`Express запущен на http://localhost:${app.get('port')};
нажмите Ctrl+C для завершения.`);
});
